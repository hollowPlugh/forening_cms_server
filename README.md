# README #

Backend code for Nackademin class JAVA15. Backend code written by Lisa Westberg. Melker Holmgren will write frontend code - not necessarily in this repo. 

### What is this repository for? ###
This is a simple CMS designed to be used by a club or small organisation. It provides management of members as well as content for a website, which won't be implemented for this project, but could easily be added.

The administrator CMS provides CRUD methods for the following entities:
    * Members
    * Links to other relevant sites
    * Events
    * Images from past events
    * Posts for a homepage

Authentication is required for all routes to CRUD methods. When an administrator has successfully logged in s/he is provided a json web token, which is valid for 24 hours.

### How do I get set up? ###
The backend code is deployed on Heroku - https://forening-cms.herokuapp.com/


### Whom do I talk to? ###

* Lisa Westberg hollowplugh@gmail.com
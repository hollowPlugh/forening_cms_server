'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let moment = require('moment');

let EventSchema = new Schema({
  title: {
    type: String,
    trim: true,
    default: 'New Event'
  },
  description: {
    type: String,
    trim: true,
    default: ''
  },
  dateTimeStart: {
    type: Date,
    default: moment()
  },
  dateTimeEnd: {
    type: Date,
    default: moment()
  },
  location: {
    address1: {
      type: String,
      trim: true,
      default: ''
    },
    address2: {
      type: String,
      trim: true,
      default: ''
    },
    postcode: {
      type: String,
      trim: true,
      default: ''
    },
    city: {
      type: String,
      trim: true,
      default: ''
    }
  }
});


module.exports = mongoose.model('Event', EventSchema);
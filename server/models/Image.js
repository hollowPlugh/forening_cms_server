'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let ImageSchema = new Schema({
  originalName: {
    type: String,
    default: '',
    trim: true
  },
  dbName: {
    type: String,
    default: '',
    trim: true
  },
  url:{
    type: String,
    default: ''
  },
  _event: { type: mongoose.Schema.Types.ObjectId, ref: 'Event' }
});

module.exports = mongoose.model('Image', ImageSchema);

'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let LinkSchema = new Schema({
  name: {
    type: String,
    trim: true,
    default: ''
  },
  url: {
    type: String,
    trim: true,
    default: ''
  }
});

module.exports = mongoose.model('Link', LinkSchema);

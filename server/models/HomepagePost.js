'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let moment = require('moment');

let HomepageSchema = new Schema({
  created: {
    type: Date,
    default: moment()
  },
  lastUpdated: {
    type: Date,
    default: moment()
  },
  title: {
    type: String,
    default: ''
  },
  body: {
    type: String,
    default: ''
  },
  isVisible: {
    type: Boolean,
    required: true,
    default: false
  }
});

module.exports = mongoose.model('HompagePost', HomepageSchema);

'use strict';

/**
 * Configure Passport strategy for authenticating users
 */

const passportJWT = require("passport-jwt");
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;
const Member = require('../models/Member');


let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader();
jwtOptions.secretOrKey = process.env.JWT_SECRET;
jwtOptions.passReqToCallback = true;

let strategy = new JwtStrategy(jwtOptions, function (req, jwt_payload, next) {
  Member.findOne({'_id': jwt_payload.id})
    .then(function (member) {
      if (!member) {
        next(null, false);
      } else {
        next(null, member);
      }
    })
    .catch((err) => {
      next(null, false, err);
    });
});

module.exports = strategy;

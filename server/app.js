'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');
const path = require('path');
const schedule = require('node-schedule');
const passport = require('passport');
const appStrategy = require('./config/strategyConfig');
const auth = require('./controllers/auth');
const app = express();

//global.env = process.env.NODE_ENV || 'development';

/**
 * Load Mongoose Models
 */
require('./models/Member');
require('./models/Image');
require('./models/Token');
require('./models/HomepagePost');
require('./models/Event');
require('./models/Link');

/**
 * Configure Passport authentication
 */
passport.use(appStrategy);
passport.serializeUser(function (user, done) {
  done(null, user._id);
});
passport.deserializeUser(function (id, done) {
  Member.findById(id).then(function (user) {
    done(null, user);
  });
});

/**
 * Middleware
 */
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:5000');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Token, x-parse-application-id, x-parse-rest-api-key, x-parse-session-token, Authorization');
  next();
});

/**
 * Initialize Passport
 */
app.use(passport.initialize());
app.use(require('./config/sessionStore').config);
app.use(passport.session());

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//This can be un-commented when file-upload is in place
/*app.use(formidable({
  encoding: 'utf-8',
  multiples: true,
  keepExtensions: true,
  hash: false
}));*/

/**
 * Load Express routes
 */
let routes = require('./routes');
app.use('/', routes);
// End middleware =====================

/**
 * Clear out all the old tokens every 23 hours
 */
schedule.scheduleJob('23 * * *', async function () {
  await auth.deleteOldTokens();
});

module.exports = app;
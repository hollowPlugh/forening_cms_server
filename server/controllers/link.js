'use strict';

const Link = require('../models/Link');
const DB_ERR_MSG = 'Internal database error';
const utils = require('../config/utils');

/**
 * Creates a new Link
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.create = async function(req, res) {
  //let body = req.fields || null;
  console.log('INSIDE LINKS/CREATE');
  let body = req.body || null;
  if(!body || !body.name || !body.url){
    return res.status(400).json({message : 'Both name and URL required'});
  }
  utils.printObjectProperties(body);
  let link = new Link(body);
  let newlink = await link.save()
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  return res.status(201).json({link: newlink});
};

/**
 * Returns a link with the given doc ID
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.getOneById = async function(req, res) {
  //let body = req.fields || null;
  let body = req.body || null;
  if(!body && !body.id) {
    return res.status(400).json({message : 'Link ID required.'});
  }
  let link = await Link.findOne({'_id': body.id})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  if(!link){
    return res.status(400).json({message : 'No such link with ID ' + body.id});
  }
  return res.status(200).json({link: link});
};

/**
 * Updates and existing Link
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.update = async function(req, res){
  //let body = req.fields || null;
  let body = req.body || null;
  if(!body || !body.id || (!body.name && !body.url)){
    return res.status(400).json({message : 'Name and url required'})
  }
  let link = await Link.findOne({'_id': body.id})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  if(!link){
    return res.status(400).json({message : 'No such link with ID ' + body.id});
  }
  link.name = body.name;
  link.url = body.url;
  let savedLink = await link.save()
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  return res.status(200).json({link:savedLink});
};

/**
 * Deletes a Link with the given doc ID
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.remove = async function(req, res) {
  //let body = req.fields || null;
  let body = req.body || null;
  if(!body || !body.id){
    return res.status(400).json({message : 'ID required'})
  }
  let link = await Link.findOne({'_id': body.id})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  if(!link){
    return res.status(400).json({message : 'No link exists with ID ' + body.id})
  }
  await link.remove()
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  return res.status(200).json({message : 'Link deleted.'})
};

/**
 * Returns all existing Links
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.getAll = async function(req, res){
  let links = await Link.find({})
    .catch(err => {
      console.log(err);
      return res.status(500).json({message : DB_ERR_MSG});
    });
  return res.status(200).json({links: links});
};
'use strict';

/**
 * CRUD-functions for Images
 */

const Image = require('../models/Image');
const Event = require('../models/Event');
const fs = require('fs');
const aws = require('./aws');
const ObjectId = require('mongoose').Types.ObjectId;
const utils = require('../config/utils');

const DB_ERR_MSG = 'Internal database error';
const AWS_BASE_URL = process.env.AWS_BASE_URL;

/**
 * Creates a new Image
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.create = async function (req, res) {
  let uploaded = req.files; // Uploaded files are in the files field of the request.
  let fields = req.fields; // Other fields in the request
  if (!uploaded || !uploaded.file || !Object.keys(uploaded)) {
    return res.status(400).json({message: "Image file(s) required"});
  }
  if (!fields || !fields.eventId) {
    return res.status(400).json({message: "EventId required"});
  }
  let eventId = fields.eventId;
  let event = await Event.findOne({'_id': eventId}, function (err) {
    if (err) {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    }
  });
  if (event.isNullOrUndefined) {
    console.log('No event');
    return res.status(400).json({message: "No event with ID " + eventId});
  }
  let files = uploaded.file;
  var savedImage = null;
  let filetype;

  // If there is more than one file, iterate through and save them.
  // Files that are not images are passed over.
  var imageCount = 0;
  if (Array.isArray(files)) {
    files.forEach(async function (file) {
      imageCount = imageCount + 1;
      try {
        filetype = file.type;
        if (!isImage(filetype)) {
          return res.status(400).json({message: 'Only images can be uploaded'});
        }
        savedImage = await createNewImage(file, fields.eventId);
        if (savedImage.err) {
          return res.status(500).json({message: DB_ERR_MSG});
        }
        savedImage = null;
      }catch(error){
        console.log('error: ' + error);
        return res.status(500).json({message: DB_ERR_MSG});
      }
    });
  } else {
    //Only one file has been uploaded. Save it.
    filetype = files.type;
    if (!isImage(filetype)) {
      return res.status(400).json({message: 'Only images can be uploaded'});
    }
    savedImage = await createNewImage(files, eventId);
    if (savedImage.err) {
      return res.status(500).json({message: savedImage.err});
    }
    imageCount = 1;
  }
  return res.status(201).json({message: imageCount + ' image(s) saved.'});
};

/**
 * Removes one image with the given doc ID
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.removeOneById = async function (req, res) {
  let body = req.fields || null;
  if (!body && !body.id) {
    return res.status(400).json({message: 'Image ID required.'});
  }
  let query = {"_id": body.id};
  let image = await Image.findOne(query).catch(err => {
    return res.status(500).json({message: err.message});
  });
  let filename = body.id + image.url.substring(image.url.lastIndexOf('.'));
  let isSuccessful = aws.removeFromBucket(filename);
  if (!isSuccessful) {
    console.log("Unable to delete from bucket: " + filename);
  }
  await Image.remove(query)
    .catch(err => {
      return res.status(500).json({message: err.message});
    });
  return res.status(200).json({message: 'Image removed.'});
};

/**
 * When an event is deleted, its related images are also deleted.
 * @param eventId String
 * @returns {Promise}
 */
exports.handleDeletedEvent = async function (eventId) {
  return handleRemoveImagesForEvent(eventId);
};

exports.removeAllForEvent = async function (req, res) {
  let body = req.fields || null;
  if (!body && !body.eventId) {
    return res.status(400).json({message: 'Event ID required.'});
  }
  let result = handleRemoveImagesForEvent(body.eventId);
  if (result.error) {
    return res.status(500).json(result);
  }
  return res.status(200).json(result);
};

/**
 * Deletes images from AWS S3
 * @param eventId String
 * @returns {Promise}
 */
async function handleRemoveImagesForEvent(eventId) {
  let returnObj = {};
  let query = {"_event": new ObjectId(eventId)};
  let images = await Image.find(query)
    .catch(err => {
      console.log(err);
      returnObj.error = DB_ERR_MSG;
      return returnObj;
    });
  if (images) {
    if (images && images.length && images.length > 0) {
      images.forEach(async function (image) {
        let removedFromBucket = aws.removeFromBucket(image.dbName);
        if (!removedFromBucket) {
          returnObj.error = 'Unable to remove ' + image.dbName + 'from bucket';
        }
        await image.remove().catch(err => {
          console.log(err);
          returnObj.error = err.message;
        });
      });
    }
    if (!returnObj.error) {
      returnObj.message = 'Image(s) removed.';
    }
    return returnObj;
  }
}

/**
 * Returns all existing Images
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.getAll = async function (req, res) {
  let allEvents = await Event.find({})
    .catch(err => {
      console.log(err);
      return res.sendStatus(500).json({message: DB_ERR_MSG});
    });
  if (!allEvents || !allEvents.length > 0) {
    return res.status(200).json({});
  }

  let imagesForEvent = [];
  let eventImages = [];
  for (let i = 0; i < allEvents.length; i++) {
    let evnt = allEvents[i];
    imagesForEvent = await getImagesforEvent(evnt);
    if (imagesForEvent.error) {
      return res.sendStatus(500).json({message: DB_ERR_MSG});
    }
    if (imagesForEvent.image_set) {
      eventImages.push(imagesForEvent);
    }
  }
  if (eventImages && eventImages.length && eventImages.length > 0) {
    console.log(eventImages);
    return res.status(200).json({images: eventImages});
  } else {
    return res.sendStatus(500).json({message: DB_ERR_MSG});
  }
};

/**
 * Returns existing images for a given Event doc ID
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.getAllByEventId = async function (req, res) {
  let body = req.fields || null;
  if (!body && !body.eventId) {
    return res.status(400).send('Event ID required.');
  }
  let query = {"_id": body.eventId};
  let event = await Event.findOne(query)
    .catch(err => {
      console.log(err);
      return res.sendStatus(500).json({message: DB_ERR_MSG});
    });
  if (!event) {
    return res.status(400).send('No event with given Id');
  }
  let eventImages = await getImagesforEvent(event);
  return res.status(200).json({images: eventImages});
};

/**
 * Collects images for an event
 * @param event Event
 * @returns {Promise}
 */
async function getImagesforEvent(event) {
  let returnObj = {};
  let query = {"_event": new ObjectId(event._id)};
  let images = await Image.find(query)
    .catch(err => {
      console.log(err);
      returnObj.error = {error: err};
    });
  if (images && images.length && images.length > 0) {
    returnObj.image_set = {event: event, images: images};
  }
  return returnObj;
}

/**
 * Saves a new image to the S3 bucket
 * @param file Binary file
 * @param eventId String
 * @returns {Promise}
 */
async function createNewImage(file, eventId) {
  let newImageDoc = new Image();
  let savedImageDoc = await newImageDoc.save()
    .catch(err => {
      console.log(err);
      returnVal.err.message = 'Failed to create new image.';
      return returnVal;
    });
  let imageId = newImageDoc._id;
  let filename = file.name;
  let fileExt = filename.substring(filename.lastIndexOf('.'));
  let newFilename = imageId + fileExt;
  let returnVal = {};
  let isSuccessful = aws.saveToBucket(newFilename, file);
  if (!isSuccessful) {
    returnVal.err.message = 'Failed to save new image to bucket.';
    return returnVal;
  }
  savedImageDoc.originalName = filename;
  savedImageDoc.dbName = newFilename;
  savedImageDoc.url = AWS_BASE_URL + newFilename;
  savedImageDoc._event = eventId;

  await savedImageDoc.save(function (err, savedImage) {
    if (err) {
      returnVal.err.message = 'Unable to save image to database';
      return returnVal;
    }
    returnVal.image = savedImage;
  });
  if (returnVal.image && returnVal.image._event) {
    returnVal.image.populate('_event');
  }
  return returnVal;
}

function isImage(typeStr) {
  return typeStr.includes('image');
}


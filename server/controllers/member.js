'use strict';

/**
 * CRUD-methods for the Member entity
 */

const Member = require('../models/Member');
const mailer = require('./mailer');
const DB_ERR_MSG = 'Internal database error';

/**
 * Creates a new Member
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.create = async function (req, res) {
  let body = req.fields || null;
  if (body) {
    let validation = validateMemberRequest(body);
    if (validation && validation.errors) {
      return res.status(400).json({message: validation.errors});
    }
  }
  let query = {"email": body.email.toLowerCase()};
  let exists = await Member.findOne(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if (exists) {
    return res.status(400).json({message: 'There is already a user with the login ' + body.email});
  }
  let member = new Member(body);
  if (member.isAdmin) {
    let newPassword = mailer.sendNewPassword(member);
    if (newPassword) {
      member.hash = Member.encryptPassword(newPassword);
    } else {
      return res.status(500).json({message: 'Unable to create and/or send password to admin'});
    }
  }
  let newMember = await member.save()
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  console.log('Member created: ' + newMember.email);
  newMember.hash = undefined;
  res.status(201).json({member: newMember});
};

/**
 * Returns an existing member with the given email address.
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.getOneByEmail = async function (req, res) {
  let body = req.fields || null;
  if (!body || !body.email) {
    return res.status(400).json({message: 'Email required'});
  }
  let query = {"email": body.email.toLowerCase()};
  let member = await Member.findOne(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if (!member) {
    return res.status(400).json({message: 'There is no member with login ' + body.email.toLowerCase});
  }
  member.hash = undefined;
  return res.status(200).json({member: member});
};

/**
 * Returns an existing member with the given doc ID
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.getOneById = async function (req, res) {
  let body = req.fields || null;
  if (!body || !body.id) {
    return res.status(400).json({message: 'Email required.'});
  }
  let query = {"_id": body.id};
  let member = await Member.findOne(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if (!member) {
    return res.status(400).json({message: 'No document found'});
  }
  member.hash = undefined;
  return res.status(200).json({member: member});
};

/**
 * Updates an existing member with the given doc ID.
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.update = async function (req, res) {
  let body = req.fields || null;
  if (!body || !body.id) {
    return res.status(400).json({message: 'Member ID required'});
  }
  let validation = validateMemberRequest(body);
  if (validation && validation.errors) {
    return res.status(400).json({message: validation.errors});
  }
  let query = {"_id": body.id};
  let member = await Member.findOne(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if (!member) {
    return res.status(400).json({message: 'No member found with ID ' + body.id});
  }
  member = updateMember(member, body);
  if(!member){
    return res.status(400).json({message: 'Unable to update member'});
  }
  var updated = await member.save()
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  member.hash = undefined;
  return res.status(200).json({member: updated});
};

/**
 * Removes an existing Member with the given doc ID
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.remove = async function (req, res) {
  let body = req.fields || null;
  if (!body || !body.id) {
    return res.status(400).json({message: 'Member ID required'});
  }
  let query = {"_id": body.id};
  await Member.remove(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  return res.status(200).json({message: 'Member removed'});
};

/**
 * Returns all members marked as administrators
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.getAllAdmins = async function (req, res) {
  let query = {"isAdmin": true};
  let admins = await Member.find(query)
    .catch(err => {
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if (admins && admins.length && admins.length > 0) {
    admins.forEach(function (admin) {
      admin.hash = undefined;
    });
  }
  return res.status(200).json({members: admins});
};

/**
 * Returns all members
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.getAllMembers = async function (req, res) {
  let members = await Member.find({})
    .catch(err => {
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if(members && members.length && members.length > 0) {
    members.forEach(function (member) {
      member.hash = undefined;
    });
  }
  return res.status(200).json({members: members});
};

/**
 * Replaces an administrator's current password with a new, supplied password
 * @param req HttpRequest
 * @param res HttpResponse
 */
exports.changePassword = async function (req, res) {
  let body = req.fields || null;
  if (!body || !body.email || !body.oldPassword || !body.newPassword) {
    return res.status(400).json({message: 'Email, old password and new password required'});
  }
  let email = body.email;
  let oldPassword = body.oldPassword;
  let newPassword = body.newPassword;
  let query = {'email': email, 'isAdmin': true};
  let member = await Member.findOne(query)
    .catch(err => {
      console.log(err);
      return res.status(500).json({message: DB_ERR_MSG});
    });
  if (!member) {
    return res.status(401).json({message: 'No admin with email ' + email});
  } else if (member) {
    if (!member.validatePassword(oldPassword)) {
      return res.status(401).json({message: 'Authentication failed.'});
    } else {
      member.setPassword(newPassword);
      await member.save()
        .catch(err => {
          console.log(err);
          return res.status(500).json({message: DB_ERR_MSG});
        });
      return res.status(200).json({message: 'Password changed.'});
    }
  }
};

/**
 * Validates body values for updating and creating a Member
 * @param body Body field of an HttpRequest
 */
function validateMemberRequest(body) {
  let message = '';
  if (!body.email) {
    message = message + 'Email is required. ';
  }
  if (!body.firstName) {
    message = message + 'First name is required. ';
  }
  if (!body.lastName) {
    message = message + 'Last name is required. ';
  }
  if (!body.isAdmin) {
    message = message + 'isAdmin is required. '
  }
  if (message) {
    //let re = /\./gi;
    //return {'errors': message.replace(re, '\r\n')}
    return {'errors': message}
  } else {
    return {}
  }
}

/**
 * Private function for updating a Member
 * @param member Member object
 * @param body Body field of an HttpRequest
 */
function updateMember(member, body) {
  member.firstName = body.firstName;
  member.lastName = body.lastName;
  member.email = body.email.toLowerCase();
  member.phone = body.phone || '';
  member.title = body.title || '';

  if (member.isAdmin === true && body.isAdmin === 'false') {
    member.isAdmin = false;
    member.hash = undefined;
  } else if (member.isAdmin === false && body.isAdmin === 'true') {
    member.isAdmin = true;
    let newPassword = mailer.sendNewPassword(member);
    if (newPassword) {
      member.hash = Member.encryptPassword(newPassword);
    } else {
      return null;
    }
  }
  return member;
}